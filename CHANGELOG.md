# Changelog
## Unreleased

### Changed

- Ne plus utiliser le pipeline `formidable_traitements` pour mettre le traitement à la fin, mais s'appuyer sur le fait que Formidable v7 trie automatiquement l'ordre des traitements en fonction de leurs dépendances

## 1.3.0 - 2025-01-24

### Added

- Compatible formidable participation v6

### Fixed


- Éviter une fatal au cron s'il n'y a pas de liste
- Ne pas faire planter si jamais la liste a été détruite entre temps
- Le choix de la liste est obligatoire (si jamais la liste est détruite après coup)

## 1.2.0 - 2024-08-25

### Fixed

- Compatible formidable participation 5.0

## 1.0.3 - 2024-02-23

### Changed

- Indiquer par `necessite` la dépendance au traitement `formidable_participation`
