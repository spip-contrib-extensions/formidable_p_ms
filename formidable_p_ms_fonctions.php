<?php



/**
 * Donne un titre de segment à partir de l'évènement auquel il correspond
 * @param int $id_evenement
 * @return string
**/
function formidable_p_ms_titrer_segment_evenement(int $id_evenement): string {
	if ($id_evenement < 1) {
		return _T('formidable_p_ms:titre_segment_sans_evenement');
	}
	$titre = '';
	$infos_evenement = sql_fetsel('titre, horaire, date_debut, date_fin, id_evenement', 'spip_evenements', "id_evenement=$id_evenement");
	if ($infos_evenement) {//Securité d'appel
		$date_formatee = $infos_evenement['date_debut'] . ' -> ' . $infos_evenement['date_fin'];// Au format SQL, pour faciliter le tri dans les colonnes
		$titre = "$date_formatee (".$infos_evenement['titre'].' - '. strtolower(_T('info_numero_abbreviation'))."$id_evenement)";
	}
	return $titre;
}
