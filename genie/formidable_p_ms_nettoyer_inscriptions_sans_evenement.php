<?php

/**
 * Inspecte les différentes listes et supprime les inscriptions qui ne sont liées à aucun evenement
 * @param int $t
 * @return int
**/
function genie_formidable_p_ms_nettoyer_inscriptions_sans_evenement_dist(int $t): int {
	include_spip('inc/config');
	include_spip('base/abstract_sql');
	$precedent = lire_config('formidable_p_ms_jobs/nettoyer_segment_sans_evenement/id_mailsubscribinglist') ?? 0;
	$id_mailsubscribinglist = sql_getfetsel('id_mailsubscribinglist', 'spip_mailsubscribinglists', "id_mailsubscribinglist > $precedent", '', 'id_mailsubscribinglist', '0,1');
	if (!$id_mailsubscribinglist) {//Si on a fait le tour des listes, on recommence
		$id_mailsubscribinglist =  sql_getfetsel('id_mailsubscribinglist', 'spip_mailsubscribinglists', "id_mailsubscribinglist > 0", '', 'id_mailsubscribinglist', '0,1');
	}
	$nettoyer = charger_fonction('nettoyer_inscriptions_sans_evenement', 'formidable_p_ms');
	$nettoyer(intval($id_mailsubscribinglist));
	ecrire_config('formidable_p_ms_jobs/nettoyer_segment_sans_evenement/id_mailsubscribinglist', $id_mailsubscribinglist);

	return 1;
}
