# formidable_participation_mailsubscriber

Un plugin qui se combine au plugin formidable_participation.

Il permet à une personne s'inscrivant à un évènement via formidable d'être inscrite automatiquement à un segment de liste regroupant toutes les personnes participants à l'évènement.