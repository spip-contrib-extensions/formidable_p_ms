<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'id_evenement_label' => 'Identifiant de l\'évènement',
	'id_evenement_titre' => 'Évènement(s) associé(s)',
	'id_evenement_explication' => 'Identifiant de l\'évènement auquel la personne est inscrite. Ce champ est verrouillé car il est réglé automatiquement par les formulaires Formidable d\'inscription aux évènements. Si -1, le segment de liste regroupe les personnes inscrite à aucun évènement',
	'remplissage_en_cours' => 'Remplissage de la liste depuis les précédentes réponses en cours (@pourcent@ %)',
	'remplissage_acheve' => 'Remplissage de la liste depuis les précédentes réponses achevé',
	'remplissage_non_lance' => 'Remplissage de la liste non lancé',
	'traiter_p_ms_titre' => 'Inscrire à un segment de liste pour l\'évènement',
	'traiter_p_ms_description' => 'Inscrire à un segment de liste pour l\'évènement',
	'traiter_p_ms_option_id_liste_label' => 'Liste de diffusion',
	'id_formulaire_p_ms_label' => 'Formulaire d\'inscription aux évènements lié à cette liste',
	'titre_segment_sans_evenement' => 'Sans inscription à un évènement',
	'comment_optin' => 'Inscription à l\'évènement @id_evenement@ via le formulaire @id_formulaire@',

];
