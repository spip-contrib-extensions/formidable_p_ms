<?php



/**
 * Crée, si besoin, des segments, dans la liste concernée, correspondant à l'évènement
 * @param int $id_mailsubscribinglist
 * @param int $id_evenement
 * @return int;
 **/
function formidable_p_ms_creer_segment_dist(int $id_mailsubscribinglist, int $id_evenement): int {
	include_spip('inc/sql');


	include_spip('inc/autoriser');
	include_spip('action/editer_objet');
	include_spip('formidable_p_ms_fonctions');

	static $segments;
	// Récuperer les segments,  le faire qu'une fois par hit
	if (!is_array($segments)) {
		$segments = @unserialize(sql_getfetsel('segments', 'spip_mailsubscribinglists', "id_mailsubscribinglist=$id_mailsubscribinglist"));
		if (!$segments) {
			$segments = [];
		}
	}

	// Si on a deja un segment pour cet id_evenement, on ne se fatigue pas
	$ids_evenement_en_base = array_column($segments, 'filtre_id_evenement');
	$id_segment = array_search($id_evenement, $ids_evenement_en_base, false);
	if ($id_segment !== false) {
		return $id_segment;
	}

	// Sinon on continue notre travail
	if ($id_evenement > -1) {
		$titre = formidable_p_ms_titrer_segment_evenement($id_evenement);
	} else {
		$titre = _T('formidable_p_ms:titre_segment_sans_evenement');
	}

	if (!empty($segments)) {
		$id_segment = max(array_keys($segments)) + 1;
	} else {
		$id_segment = 1;
	}
	$segment = [
		'id' => $id_segment,
		'titre' => $titre,
		'auto_update' => 'oui',
		'filtre_id_evenement' => "$id_evenement",
	];
	$segments[$id_segment] = $segment;

	// Enregistrer en base, y compris si c'est 1 internaute qui s'inscrit
	autoriser_exception('modifier', 'mailsubscribinglist', $id_mailsubscribinglist);
	objet_modifier('mailsubscribinglist', $id_mailsubscribinglist, ['segments' => serialize($segments)]);
	autoriser_exception('modifier', 'mailsubscribinglist', $id_mailsubscribinglist, false);
	return $id_segment;
}
