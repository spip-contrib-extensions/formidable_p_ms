<?php

/**
 * Pour une liste donnée, recherche le segment des inscriptions sans evenements
 * et supprime les inscriptions correspondantes
 * @param int
 * @return void
 **/
function formidable_p_ms_nettoyer_inscriptions_sans_evenement(int $id_mailsubscribinglist): void{
	include_spip('base/abstract_sql');
	$liste = sql_fetsel('id_mailsubscribinglist, identifiant,  segments', 'spip_mailsubscribinglists', "id_mailsubscribinglist = $id_mailsubscribinglist");
	$segments = @unserialize($liste['segments']);
	if (!$segments) {
		return;
	}
	$unsubscribe = charger_fonction('unsubscribe', 'newsletter');
	$id_mailsubscribinglist = $liste['id_mailsubscribinglist'];
	$options = [
		'listes' => [$liste['identifiant']],
		'notify' => false,
		'comment' => _T('formidable_p_ms:titre_segment_sans_evenement'),
	];
	foreach ($segments as $id => $seg) {
		if ($seg['filtre_id_evenement'] == -1) {
			$id_segment = $seg['id'];
			$res = sql_select('email', 'spip_mailsubscribers AS subscribers JOIN spip_mailsubscriptions AS subscriptions', "subscribers.id_mailsubscriber = subscriptions.id_mailsubscriber AND `id_mailsubscribinglist`=$id_mailsubscribinglist AND `id_segment` = $id_segment");
			while ($row = sql_fetch($res)) {
				$email = $row['email'];
				$unsubscribe($email, $options);
			}
			include_spip('inc/mailsubscribinglists');
			mailsubscribers_start_update_mailsubscribinglist_segment($id_mailsubscribinglist, $id_segment);//Au cas où, on reforce l'actualisation du segment. Tant pis, ce sera pour le prochain round le nettoyage ! Mais normalement si les admin ne font pas de betise, cela ne devrait pas arriver...
		}
	}
}
