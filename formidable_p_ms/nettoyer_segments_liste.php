<?php



/**
 * Pour une liste
 * Recherche les segments spécifiques à formidable_p_ms
 * Et les supprime
 * @param int $id_mailsubscribinglist
 * @return void
**/
function formidable_p_ms_nettoyer_segments_liste_dist(int $id_mailsubscribinglist): void {

	$segments = @unserialize(sql_getfetsel('segments', 'spip_mailsubscribinglists', "`id_mailsubscribinglist`=$id_mailsubscribinglist"));
	if (!$segments) {
		return;
	}

	// Charger l'action et autoriser temporairement les modifs
	$action = charger_fonction('supprimer_mailsubscribinglist_segment', 'action');
	include_spip('inc/autoriser');
	autoriser_exception('modifier', 'mailsubscribinglist', $id_mailsubscribinglist);//Par précaution

	// Parcourir le tableau des segments
	foreach ($segments as $seg => $description) {
		if (isset($description['filtre_id_evenement']) and $description['filtre_id_evenement']) {
			$action($id_mailsubscribinglist, $description['id']);
		}
	}
	autoriser_exception('modifier', 'mailsubscribinglist', $id_mailsubscribinglist, false);

	return;
}
