<?php

// Sécurité

/**
 * La fonction de traitement participation/mailsubscriber
 * Elle ne fait rien en elle même : on se branche en effet sur le pipeline spécifique à formidable participation.
 * @param array $args les options du traitement
 * @param array $retours les résultats des précédents traitements
 * @return array
**/
function traiter_p_ms_dist(array $args, array $retours) : array {
	$retours['traitements']['p_ms'] = true;
	return $retours;
}
