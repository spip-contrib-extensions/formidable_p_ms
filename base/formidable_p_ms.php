<?php

/**
 * Déclarer un champ extra `id_formulaire` sur l'objet mailsubscribinglist.
 * Ce champ extra est automatiquement synchronisée lorsqu'un traitement `p_ms` est activé sur un formulaire.
**/
function formidable_p_ms_declarer_champs_extras(array $champs = array()): array {
	$champs['spip_mailsubscribinglists']['id_formulaire_p_ms'] = [
		'saisie' => 'hidden',
		'options' => [
			'nom' => 'id_formulaire_p_ms',
			'defaut' => '',
			'label' => _T('formidable_p_ms:id_formulaire_p_ms_label'),
			'sql' => 'BIGINT DEFAULT NULL',
			'versionner' => true,
			'traitements' => 'formidable_p_ms_id_formulaire_p_ms_traitements(%s)',
		]
	];
	return $champs;
}
/**
 * Formate l'affichage du champ extra `id_formulaire_p_ms`
 * @param int|null $id_formulaire
 * @return string
**/
function formidable_p_ms_id_formulaire_p_ms_traitements($id_formulaire): string {
	if (!$id_formulaire) {
		return '';
	}
	include_spip('inc/filtres');
	$texte = generer_info_entite($id_formulaire, 'formulaire', 'titre')." ($id_formulaire)";
	if (test_espace_prive()) {
		include_spip('inc/formidable_p_ms');
		$id_mailsubscribinglist = sql_getfetsel('id_mailsubscribinglist', 'spip_mailsubscribinglists', "id_formulaire_p_ms=$id_formulaire");
		$picto = formidable_p_ms_picto_remplissage($id_mailsubscribinglist, $id_formulaire);
		$texte = propre("[$texte->".generer_url_ecrire('formulaire', "id_formulaire=$id_formulaire")."] $picto");

	}
	return $texte;
}
