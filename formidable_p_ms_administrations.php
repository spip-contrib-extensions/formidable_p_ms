<?php



include_spip('base/formidable_p_ms');
include_spip('inc/cextras');
/**
 * Fonction d'installation du plugin et de mise à jour.
 **/
function formidable_p_ms_upgrade($nom_meta_base_version, $version_cible) {
	$maj = ['create' => []];
	cextras_api_upgrade(formidable_p_ms_declarer_champs_extras(), $maj['create']);
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de desinstallation du plugin
**/
function formidable_p_ms_vider_tables($nom_meta_base_version) {
	include_spip('inc/sql');
	include_spip('action/editer_objet');
	include_spip('formidable_fonctions');

	// Supprimer les traitements spécifiques
	$res = sql_select('id_formulaire, traitements', 'spip_formulaires');
	while ($row = sql_fetch($res)) {
		$id_formulaire = $row['id_formulaire'];
		$traitements = formidable_deserialize($row['traitements']);
		if ($traitements and isset($traitements['p_ms'])) {
			unset($traitements['p_ms']);
			$traitements = formidable_serialize($traitements);
			objet_modifier('formulaire', $id_formulaire, ['traitements' => $traitements]);
		}
	}
	// Supprimer les segments spécifiques sur les listes
	$res = sql_select('id_mailsubscribinglist', 'spip_mailsubscribinglists');
	$nettoyer_segments_liste = charger_fonction('nettoyer_segments_liste', 'formidable_p_ms');
	while ($row = sql_fetch($res)) {
		$nettoyer_segments_liste($row['id_mailsubscribinglist']);
	}

	// Supprimer les champs extras spécifiques
	cextras_api_vider_tables(formidable_p_ms_declarer_champs_extras());
	include_spip('inc/config');
	effacer_config('formidable_p_ms_jobs');
	effacer_meta($nom_meta_base_version);
}
