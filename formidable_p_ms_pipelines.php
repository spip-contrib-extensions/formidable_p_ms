<?php





/**
 * Après l'enregistrement d'une participation à un évènement, inscrire à la liste de diffusion associé au formulaire
 * @param array $flux
 * @return array $flux
**/
function formidable_p_ms_traiter_formidableparticipation(array $flux) : array {
	include_spip('formidable_fonctions');
	// Initialisation
	$args = $flux['args'];
	//Sécurité d'appel
	if (!isset($args['email'])) {
		return $flux;
	}
	// Modifier les infos d'inscription à la liste que si on demande explicitement une participation ou bien qu'on desinscrit d'un evenement
	$choix_participation = $args['choix_participation'] ?? '';
	if ($choix_participation !== 'oui'
		and (!isset($flux['args']['evenements_participants_anciens']) or !$flux['args']['evenements_participants_anciens'])
	) {
		return $flux;
	}


	// Recherche des traitements
	$traitements = formidable_deserialize(sql_getfetsel('traitements', 'spip_formulaires', '`id_formulaire`='. $args['id_formulaire']));//En static, pour éviter les requetes SQL multiples si plusieurs inscriptions
	if (!isset($traitements['p_ms']) or empty($traitements['p_ms']['id_liste'])) {// On ne continue que si le traitement participation_ms est actif + au moins une liste cochée.
		return $flux;
	}
	// Créer un segment si jamais on a un evenement
	$creer_segment = charger_fonction('creer_segment', 'formidable_p_ms');
	if (isset($flux['args']['id_evenement']) and $flux['args']['id_evenement']) {
		$id_segment = $creer_segment($traitements['p_ms']['id_liste'], $flux['args']['id_evenement']);
	}

	// Par sécurité, tjr s'assurer d'avoir le segment -1
	$creer_segment($traitements['p_ms']['id_liste'], -1);

	// Options de souscription
	$options_subscribe = [];

	// Pour NL nom = prenom + nom
	$options_subscribe['nom'] = $args['nom'] ?? '';
	if (isset($args['prenom']) and $args['prenom']) {
		$options_subscribe['nom'] = $args['prenom'].' '.$options_subscribe['nom'];
	}

	$email = $args['email'];
	$options_subscribe['graceful'] = false;//On permet de se reinscrire
	$options_subscribe['notify'] = false;// Pas de notification, à voir si on propose une option à terme
	$options_subscribe['lang'] = $GLOBALS['spip_lang'];// Langue par défaut lors de l'inscription : la langue courante dans la page
	$options_subscribe['force'] = true;//On force l'inscription
	$options_subscribe['statut'] = 'valide';
	$options_subscribe['listes'] = [sql_getfetsel('identifiant', 'spip_mailsubscribinglists', '`id_mailsubscribinglist`='.$traitements['p_ms']['id_liste'])];
	$options_subscribe['comment'] = _T('formidable_p_ms:comment_optin', ['id_formulaire' => $args['id_formulaire'], 'id_evenement' => $flux['args']['id_evenement']]);

	// Et maintenant, on fait
	// TO DO : souscription après le post (confirmation nospam)
	$newsletter_subscribe = charger_fonction('subscribe', 'newsletter');
	$newsletter_subscribe($email, $options_subscribe);

	// Si jamais on a changé d'email ou d'id_evenement, actualiser les segments
	if (isset($flux['args']['evenements_participants_anciens'])) {
		include_spip('inc/mailsubscribinglists');

		// Cas 1a : changement d'email
		// Cas 1b : changement de statut et donc de participation
		$emails_participants_anciens = array_column($flux['args']['evenements_participants_anciens'], 'email');
		$emails_participants_anciens = array_unique($emails_participants_anciens);
		foreach ($emails_participants_anciens as $email_ancien) {
			if ($email_ancien !== $email or $choix_participation !== 'oui') {
				$res = sql_select('id_mailsubscriber', 'spip_mailsubscribers', '`email`='.sql_quote($email_ancien));
				while ($row = sql_fetch($res)) {
					mailsubscribers_actualise_mailsubscribinglist_segments($row['id_mailsubscriber'], $traitements['p_ms']['id_liste'], true);
				}
				$nettoyer = charger_fonction('nettoyer_inscriptions_sans_evenement', 'formidable_p_ms');
				$nettoyer($traitements['p_ms']['id_liste']);
			}
		}

		// Cas 2 : changement d'évènement, dans ce cas là on actualise toute la liste pour cet email
		$id_evenement_anciens = array_column($flux['args']['evenements_participants_anciens'],'id_evenement');
		if (!in_array($flux['args']['id_evenement'], $id_evenement_anciens)) {
				$res = sql_select('id_mailsubscriber', 'spip_mailsubscribers', '`email`='.sql_quote($email));
				while ($row = sql_fetch($res)) {
					mailsubscribers_actualise_mailsubscribinglist_segments($row['id_mailsubscriber'], $traitements['p_ms']['id_liste'], true);
				}
		}

	}

	return $flux;
}

/**
 * Contourner l'absence de https://git.spip.net/spip/spip/commit/f8c7e03ec2d63cd7d82b35f9de3d5421fd28ad5c en SPIP 4
 * A supprimer donc lorsqu'on forcera SPIP 4.1 ou plus
 * @param array $flux
 * @return array
**/
function formidable_p_ms_pre_edition(array $flux): array {
	if (isset($flux['args']['table_objet']) and $flux['args']['table_objet'] === 'formulaires' and !isset($flux['args']['champs_anciens'])) {
		set_request('formulaires_traitements_anciens', sql_getfetsel('traitements', 'spip_formulaires', 'id_formulaire='.$flux['args']['id_objet']));
	}
	return $flux;
}

/**
 * À la modification des traitements,
 * rechercher la liste sur laquelle le traitement `p_ms`
 * est activée, et stocker l'id du formulaire dans le champs extra `id_formulaire_p_ms` de la liste concernée.
 *
 * À la modification d'un evenement, regarde si la date de début, la date de fin, le champ horaire ou le titre a bougé,
 * et si c'est le cas actualise le titre des segments de liste correspondant.
 * @param array $flux;
 * @return array
 **/
function formidable_p_ms_post_edition(array $flux): array {
	include_spip('formidable_fonctions');
	// Partie 1 : les modifications des traitements sur formulaire
	if ($flux['args']['table_objet'] ?? '' === 'formulaires' and isset($flux['data']['traitements'])) {
		include_spip('action/editer_objet');
		$id_formulaire = $flux['args']['id_objet'];
		// Trouver l'ancienne et la nouvelle liste
		$traitements_anciens = $flux['args']['champs_anciens']['traitements'] ?? '';
		if ($traitements_anciens === '') {//A supprimer si SPIP >= 4.1
			$traitements_anciens = _request('formulaires_traitements_anciens');
		}

		$traitements_anciens = formidable_deserialize($traitements_anciens);
		$traitements_nouveaux = formidable_deserialize($flux['data']['traitements']);
		if (!$traitements_anciens) {
			$traitements_anciens = [];
		}
		if (!$traitements_nouveaux) {
			$traitements_nouveaux = [];
		}

		$liste_ancienne = $traitements_anciens['p_ms']['id_liste'] ?? '';
		$liste_nouvelle = $traitements_nouveaux['p_ms']['id_liste'] ?? '';
		if ($liste_nouvelle != $liste_ancienne) {
			include_spip('inc/autoriser');

			if ($liste_nouvelle) {
				autoriser_exception('modifier', 'mailsubscribinglist', $liste_nouvelle);
				objet_modifier('mailsubscribinglist', $liste_nouvelle, ['id_formulaire_p_ms' => $id_formulaire]);
				autoriser_exception('modifier', 'mailsubscribinglist', $liste_nouvelle, false);
				include_spip('inc/formidable_p_ms');
				formidable_p_ms_remplissage_initial($liste_nouvelle, $id_formulaire);
			}

			if ($liste_ancienne) {
				autoriser_exception('modifier', 'mailsubscribinglist', $liste_ancienne);
				objet_modifier('mailsubscribinglist', $liste_ancienne, ['id_formulaire_p_ms' => '', 'statut' => 'poubelle']);
				autoriser_exception('modifier', 'mailsubscribinglist', $liste_ancienne, false);
				include_spip('inc/config');
				effacer_config("formidable_p_ms/remplissage_initial/$liste_ancienne/$id_formulaire");
			}


		}
	}
	// Partie 2 : les modifications des titres/date d'evenement
	if ($flux['args']['table_objet'] ?? '' === 'evenements') {
		$data = $flux['data'];
		if (
			isset($data['titre'])
			or isset($data['date_debut'])
			or isset($data['date_fin'])
			or isset($data['horaires'])
		) {
			$id_evenement = $flux['args']['id_objet'];
			$regexp = '\"filtre_id_evenement\";(s|i):\\\d*:?\"?'.$id_evenement;//Pour chercher en regexp au sein d'un tableau serializé, dans la BDD
			$res = sql_select('id_mailsubscribinglist, segments', 'spip_mailsubscribinglists', "`segments` REGEXP '$regexp'");
			while ($row = sql_fetch($res)) {
				$segments = unserialize($row['segments']);
				include_spip('formidable_p_ms_fonctions');
				$titre = formidable_p_ms_titrer_segment_evenement($id_evenement);
				foreach ($segments as $seg => &$des) {
					if (($des['filtre_id_evenement'] ?? '') == $id_evenement) {
						$des['titre'] = $titre;
					}
				}
				$segments = serialize($segments);
				include_spip('inc/autoriser');
				include_spip('action/editer_objet');
				autoriser_exception('modifier', 'mailsubscribinglist', $row['id_mailsubscribinglist']);
				objet_modifier('mailsubscribinglist', $row['id_mailsubscribinglist'], ['segments' => $segments]);
				autoriser_exception('modifier', 'mailsubscribinglist', $row['id_mailsubscribinglist'], false);
			}
		}
	}


	return $flux;
}

/**
 * Ce pipeline fait deux choses :
 * - Déclarer une saisie input readyonly pour les segments de liste
 * - Renseigner les informations pour les inscrits sur le segment de liste
 * Cf. doc https://contrib.spip.net/Utiliser-les-segments-de-listes
 * @param array $flux;
 * @return array $flux
**/
function formidable_p_ms_mailsubscriber_informations_liees(array $flux): array {
	// Déclaration de l'id evenement sur les segments de liste
	if (isset($flux['args']['declarer'])) {
		$data = &$flux['data'];
		$data['id_evenement'] = [
			'saisie' => 'formidable_p_ms_evenement',
			'titre' => _T('formidable_p_ms:id_evenement_titre'),
			'options' => [
				'nom' => 'id_evenement',
				'label' => _T('formidable_p_ms:id_evenement_label'),
				'explication' => _T('formidable_p_ms:id_evenement_explication'),
				'readonly' => 'oui',
			]
		];
	}

	// Déclaration de l'association email -> segment
	if (isset($flux['args']['email'])
		and $id_mailsubscriber = $flux['args']['id_mailsubscriber']
		and $email = $flux['args']['email']
	) {
		$res = sql_select('id_evenement', 'spip_evenements_participants','`email`='.sql_quote($email) . 'AND `reponse`=\'oui\'');
		$flux['data']['id_evenement'] = [];
		while ($raw = sql_fetch($res)) {
			$flux['data']['id_evenement'][] = $raw['id_evenement'];
		}

		// Si aucun evenement, on attribut au pseudo-evenement -1
		if (empty($flux['data']['id_evenement'])) {
			$flux['data']['id_evenement'][] = '-1';
		}
		$flux['data']['id_evenement'] = array_unique($flux['data']['id_evenement']);//Si une mêne adresse est présente plusieurs fois au meme evenement (inscriptions familiale, par ex.)
	}
	return $flux;
}

/**
 * Sur la vue du formulaire
 * complète si besoin les infos sur la synchronisation avec les listes
 * @param array $flux
 * @return array
**/
function formidable_p_ms_affiche_milieu(array $flux): array {
	include_spip('formidable_fonctions');
	if ($flux['args']['exec'] === 'formulaire' and $id_formulaire = $flux['args']['id_formulaire']) {
		$data = &$flux['data'];
		$chaine = _T('formidable_p_ms:traiter_p_ms_titre');
		if (strpos($data, $chaine)) {
			$traitements = formidable_deserialize(sql_getfetsel('traitements', 'spip_formulaires', "id_formulaire=$id_formulaire"));
			$id_mailsubscribinglist = $traitements['p_ms']['id_liste'] ?? '';
			if ($id_mailsubscribinglist) {
				include_spip('inc/formidable_p_ms');
				$picto = formidable_p_ms_picto_remplissage($id_mailsubscribinglist, $id_formulaire);
				$data = str_replace($chaine, "$chaine $picto", $data);
			}
		}
	}
	return $flux;
}
