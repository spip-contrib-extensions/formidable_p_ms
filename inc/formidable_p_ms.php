<?php

/**
 * Rempli initialement la liste `$id_mailsubscribinglist` à partir des réponses à `$id_formulaire`
 * @param int $id_mailsubscribinglist
 * @param int $id_formulaire
 * @return void;
 **/
function formidable_p_ms_remplissage_initial(int $id_mailsubscribinglist, int $id_formulaire): void {
	include_spip('inc/abstract_sql');
	include_spip('inc/config');
	// Sécurité : on verifie que la liste est tjr associée au formulaire
	if ($id_formulaire != sql_getfetsel('id_formulaire_p_ms', 'spip_mailsubscribinglists', "id_mailsubscribinglist=$id_mailsubscribinglist")) {
		effacer_config("formidable_p_ms/remplissage_initial/$id_mailsubscribinglist/$id_formulaire");
		return;
	}
	$pas = 10; // Cela semble pas mal de traiter 10 par 10 ?
	$init = false;
	// Trouver les réponses min et max du formulaire à prendre en compte
	if (!$min = lire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/min")) {
		$min = sql_getfetsel('id_formulaires_reponse', 'spip_formulaires_reponses', "id_formulaire=$id_formulaire",'', 'id_formulaires_reponse ASC', '0, 1');
		if (!$min) {
			$min = 1;
		}
		ecrire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/min", $min);
		$init = true;
	}
	if (!$max = lire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/max")) {
		$max = sql_getfetsel('id_formulaires_reponse', 'spip_formulaires_reponses', "id_formulaire=$id_formulaire",'', 'id_formulaires_reponse DESC', '0, 1');
		ecrire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/max", $max);
		$init = true;
	}
	// Remplir la liste
	// On ne remplit que si jamais
	// 1. On ne vient pas d'initialiser la jobqueue
	// 2. $min <= $max c'est à dire s'il reste des réponses à traiter
	// Le remplissage se fait via l'appel au traitement
	include_spip('formidable_fonctions');
	if (!$init and $min <= $max) {

		$formulaire = sql_fetsel('*', 'spip_formulaires', "id_formulaire = $id_formulaire");
		$traitements = formidable_deserialize($formulaire['traitements']);
		$traiter = charger_fonction('participation', 'traiter');
		$args = array(
			'formulaire'  => $formulaire,
			'id_formulaire' => $id_formulaire,
			'options' => $traitements['participation'],
			'changement_statut' => false
		);
		$retours = array(
			'traitements' => array(),
		);
		$res = sql_select('id_formulaires_reponse', 'spip_formulaires_reponses', "`id_formulaire` = $id_formulaire AND `id_formulaires_reponse` >= $min AND `id_formulaires_reponse` <= $max", '', 'id_formulaires_reponse ASC', "0, $pas");
		while ($row = sql_fetch($res)) {
			$id_formulaires_reponse = $row['id_formulaires_reponse'];
			$args['id_formulaires_reponse'] = $id_formulaires_reponse;
			$retours['id_formulaires_reponse'] = $id_formulaires_reponse;
			$traiter($args, $retours);
		}
		$min = $id_formulaires_reponse + 1;
		ecrire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/min", $min);
		// Remettre un job_queue si on n'a pas fini
	}

	// S'il y a des réponses à traiter, on (re)lance le processus
	if ($min <= $max) {
		include_spip('inc/queue');
		queue_add_job(
			'formidable_p_ms_remplissage_initial',
			"Remplissage initial de la liste $id_mailsubscribinglist depuis les réponses au formulaire $id_formulaire; par lot de $pas réponses; intervalle de réponses à traiter: $min-$max",
			[$id_mailsubscribinglist, $id_formulaire],
			'inc/formidable_p_ms',
			true,
			2//Un peu important, mais pas trop
		);
	}
}

/**
 * Renvoie le pictogramme indiquant si l'on rempli la liste avec les emails précédents
 * @param int $id_mailsubscribinglist
 * @param int $id_formulaire
 * @return string
**/
function formidable_p_ms_picto_remplissage(int $id_mailsubscribinglist, int $id_formulaire): string {
		$min = lire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/min");
		$max = lire_config("formidable_p_ms_jobs/remplissage_initial/$id_mailsubscribinglist/$id_formulaire/max");
		$picto = '';
		$chaine = '';
		include_spip('inc/utils');
		if ($min <= $max and $min and $max) {
			$pourcent = round($min*100/$max);// On est d'accord c'est hyper approximatif car on ne prend pas toutes les réponses entre min et max mais bon on va pas faire une requete sql juste pour ca
			$picto = find_in_theme('images/resyncing-16.gif');
			$chaine = _T('formidable_p_ms:remplissage_en_cours', ['pourcent' => $pourcent]);
		} elseif ($min and $max) {
			$picto = find_in_theme('images/resync-16.gif');
			$chaine = _T('formidable_p_ms:remplissage_acheve');
		} elseif (!$min and !$max) {//Cas d'erreur
			$picto = find_in_theme('images/erreur-xx.svg');
			$chaine = _T('formidable_p_ms:remplissage_non_lance');
		}
		if ($chaine) {
			$filtre = charger_fonction('balise_img', 'filtre');
			$picto = $filtre($picto, $chaine, '', 16);
			$picto = inserer_attribut($picto, 'title', $chaine);
		}
		return $picto;
}
